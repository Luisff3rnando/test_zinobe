import unittest
from conexion import SqlConexion, MongoConexion



class TestCasesCountry(unittest.TestCase):

    def sql_lite(self,):
        test = {'city': 'colombia', 'languaje': 'español', 'region': 'america'}
        obj=SqlConexion().sql_insert(test)
        self.assertIsNone(obj)

    def mongo(self,):
        test = {'city': 'colombia', 'languaje': 'español', 'region': 'america'}
        obj=MongoConexion().save_mongo(test)
        self.assertIsNone(obj)


if __name__ == '__main__':
    print('Run Test ...')
    unittest.main()
