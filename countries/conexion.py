import sqlite3
from sqlite3 import Error
from pymongo import MongoClient
import os


class SqlConexion:

    def main(self):
        self.sql_connection()

    def sql_connection(self,):
        try:
            con = sqlite3.connect('region.db')
            return con
        except Error:
            print(Error)

    def sql_table(self, con):
        cursorObj = con.cursor()
        cursorObj.execute(
            "CREATE TABLE IF NOT EXISTS  region(id integer PRIMARY KEY  AUTOINCREMENT, json_value json_text,created_at TEXT DEFAULT CURRENT_TIMESTAMP)")
        con.commit()

    def sql_insert(self, countries):
        con = self.sql_connection()
        self.sql_table(con)
        cursorObj = con.cursor()
        cursorObj.execute(
            'INSERT INTO region(json_value) VALUES(?)', (countries,))
        con.commit()


class MongoConexion:
    def __init__(self):
        self.client = MongoClient(os.environ['HOST_DB'], 27017)
        self.db = self.client['test_country']

    def close(self):
        self.client.close()

    def save_mongo(self, data):
        self.db.get_collection('region').insert_many(data)
        self.close()
