import requests
import pandas as pd
import json
import hashlib
from conexion import SqlConexion,MongoConexion


# from countries.env import env

class ServerConexion:

    def continents(self):
        url = 'https://restcountries-v1.p.rapidapi.com/all'

        headers = {
            'x-rapidapi-host': 'restcountries-v1.p.rapidapi.com',
            'x-rapidapi-key': '3e8d3d7749mshddb97ffbde785dfp1627d5jsn75b7ed3272a1'
        }
        response = requests.request("GET", url, headers=headers)
        df = pd.read_json(response.text)
        region = df['region'].unique()
        region = region[:6]
        region.sort()
        return region

    def get_country(self, region_name):
        headers = {}
        response = requests.request(
            "GET", 'https://restcountries.eu/rest/v2/region/'+region_name, headers=headers)
        if response.status_code == 200:
            time = response.elapsed.microseconds / 1000
            data = json.loads(response.text)
            c_name = data[0]['name']
            crypto = hashlib.sha1(
                data[0]['languages'][0]['name'].encode()).hexdigest()
            return c_name, crypto, time
        return 0,0,0

    def data_frame(self,):
        region = self.continents()
        city_name = []
        languaje = []
        _time = []
        for i in region:
            if i.__len__() > 0:
                c_name, list_language, time = self.get_country(region_name=i)
                city_name.append(c_name)
                languaje.append(list_language)
                _time.append(time)

        table = pd.DataFrame(
            {'Region': region, 'City Name': city_name, 'Languaje': languaje, 'Time': time})

        json_save = table.to_json(orient='records')
        # Save Database Sqllite 
        SqlConexion().sql_insert(json_save)
        MongoConexion().save_mongo(json_save)
        # Dataframe list Region
        print("------------------------------ DataFrame -------------------------------------")
        print(table)
        print("-------------------------------------------------------------------------------")
        print("--- Time Value ----")
        print(table['Time'].describe())
        print("-------------------")


ServerConexion().data_frame()
